package com.krishipahal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EnquiryForm extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    MarshMallowPermission mallowPermission;

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "KrishiPahal";
    Context context;
    ImageView enquiry_back;
    EditText edt_name,edt_address,edt_mobile,edt_problem, edt_email;
    EditText edt_fatherName, edt_pinCode;
    Spinner spinner;
    LinearLayout ll_photos;
    Button btn_submit;
    LinearLayout ll_machinery;

    String from = "";
    String name ="";
    String address = "";
    String mobile = "";
    String email = "";
    String problem = "";
    String enquiry_type = "";
    String IMAGE_PATH = " ";
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences preferences;

    JSONObject jsonObject;
    ArrayList<String> base64ImagesArray;
    ArrayList<Uri> imagesArray;
    MultipleEnquiryFormAdapter multipleEnquiryFormAdapter;
    RecyclerView grdImages;
    String[] problemsArray;
    String problemsSpinnerValue="", fatherName="", pinCode="";
    //ImageView image_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_form);
        context = this;
        mallowPermission = new MarshMallowPermission(this);
        enquiry_back = (ImageView)findViewById(R.id.enquiry_back);
        edt_name = (EditText)findViewById(R.id.edt_name);
        edt_address = (EditText)findViewById(R.id.edt_address);
        edt_mobile = (EditText)findViewById(R.id.edt_mobile);
        edt_problem = (EditText)findViewById(R.id.edt_problem);
        edt_email = (EditText)findViewById(R.id.edt_email);
        ll_photos = (LinearLayout)findViewById(R.id.ll_photos);
        btn_submit = (Button)findViewById(R.id.btn_submit);

        ll_machinery = (LinearLayout) findViewById(R.id.ll_machinery);
        edt_fatherName = (EditText)findViewById(R.id.edt_fatherName);
        edt_pinCode = (EditText)findViewById(R.id.edt_pinCode);
        spinner = (Spinner) findViewById(R.id.spinner);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        //image_view = (ImageView)findViewById(R.id.image_view);
        grdImages = (RecyclerView)findViewById(R.id.grdImages);

        enquiry_type = getIntent().getStringExtra("agro_type");
        if(enquiry_type.matches("Machinery")) {
            ll_machinery.setVisibility(View.VISIBLE);
            problemsArray = getResources().getStringArray(R.array.problems_text);
            ArrayAdapter aaaa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, problemsArray);
            aaaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(aaaa);
            spinner.setOnItemSelectedListener(this);
        } else {
            ll_machinery.setVisibility(View.GONE);
        }
        from = getIntent().getStringExtra("From");
        enquiry_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });
        base64ImagesArray = new ArrayList<>();
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("ImageArrayBase64", base64ImagesArray.size()+"");
                //showpDialog();
                validation();
                //Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show();
                //hidepDialog();
            }
        });

        ll_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askForCameraPermission();
            }
        });
    }

    private void validation() {
        name = edt_name.getText().toString();
        address = edt_address.getText().toString();
        mobile = edt_mobile.getText().toString();
        problem = edt_problem.getText().toString();
        email = edt_email.getText().toString();
        fatherName = edt_fatherName.getText().toString().trim();
        pinCode = edt_pinCode.getText().toString().trim();
        if (name.equals("")){
            Toast.makeText(context, "Enter the name...", Toast.LENGTH_SHORT).show();
        }
        if (address.equals("")){
            Toast.makeText(context, "Enter the address...", Toast.LENGTH_SHORT).show();
        }
         if (mobile.equals("")){
            Toast.makeText(context, "Enter the mobile...", Toast.LENGTH_SHORT).show();
        }
        /*if (mobile.equals("")){
            Toast.makeText(context, "Enter your email...", Toast.LENGTH_SHORT).show();
        }*/
         if (problem.equals("")){
            Toast.makeText(context, "Enter the problem...", Toast.LENGTH_SHORT).show();
        }
        if (mobile.length()<10){
            Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
        }

         else {
            if(!"".equals(email)) {
                if(!isValidEmail(email)) {
                    Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                } else {
                    try{makepostRequest();} catch (Exception e){e.printStackTrace();}
                }
            } else {
                //new uplaodImageFile().execute("");
                try{makepostRequest();} catch (Exception e){e.printStackTrace();}
            }
         }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.spinner) {
            problemsSpinnerValue = problemsArray[position];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class uplaodImageFile extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            makepostRequest();
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            hidepDialog();
        }

        @Override
        protected void onPreExecute() {showpDialog();}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


    private void makepostRequest() {
        if(base64ImagesArray.size() > 0 && base64ImagesArray != null) {
            jsonObject=new JSONObject();
            for(int i=0;i<=base64ImagesArray.size()-1;i++) {
                try {
                    jsonObject.put("params_"+i, base64ImagesArray.get(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        StringRequest postRequest = new StringRequest(Request.Method.POST, Webservices_url.Agroadvisorypost,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            Log.e("response_msg1423",""+jsonResponse.toString());

                            String status = jsonResponse.getString("status");
                            if (status.equals("200")){
                                Toast.makeText(context,"Your enquiry send successfully...",Toast.LENGTH_LONG).show();
                                hidepDialog();
                                Intent intent =new Intent(EnquiryForm.this,Home.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                            else {
                                hidepDialog();
                                Toast.makeText(context,"Something wrong!!! Please try again later...",Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("error_error",""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                params.put("name", ""+name);
                params.put("mobile", ""+mobile);
                params.put("address", ""+address);
                params.put("problem", ""+problem);
                params.put("email", ""+email);
                params.put("agrotype", ""+enquiry_type);
                if(jsonObject != null && jsonObject.length() > 0) {
                    params.put("imagefile", ""+jsonObject.toString());
                } else {
                    params.put("imagefile", "");
                }
                if(enquiry_type.matches("Machinery")) {
                    params.put("machinery_problem", ""+problemsSpinnerValue);
                    params.put("father_name", ""+fatherName);
                    params.put("pincode", ""+pinCode);
                }
                Log.d("paramsssssss", params+"");
                return params;
            }
        };
        //Volley.newRequestQueue(this).add(postRequest);
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(postRequest);
        AppController.getInstance().getRequestQueue().getCache().clear();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();}

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();}

    public void askForCameraPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((! mallowPermission.checkPermissionForExternalStorage())|| (!mallowPermission.checkPermissionForCamera() )) {
                mallowPermission.requestPermissionForExternalStorage();
                mallowPermission.requestPermissionForCamera();}
            else {
                showDialog();}}
                else {
            showDialog();}}

    public void showDialog() {
        final Dialog dialog = new Dialog(EnquiryForm.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagedialog);
        dialog.show();
        TextView camera_Roll = (TextView) dialog.findViewById(R.id.takePic);
        camera_Roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 1);
                dialog.dismiss();
            }
        });
        TextView takePicture = (TextView) dialog.findViewById(R.id.gallaryPic);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 5);
                dialog.dismiss();

                /*Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 2);
                dialog.dismiss();*/

            }
        });
        TextView textView_Cancel = (TextView) dialog.findViewById(R.id.canCel);
        textView_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title"+timestamp, null);
        return Uri.parse(path);}

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;}

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == 2 && resultCode == RESULT_OK && null != data) {

//            String[] all_path = data.getStringArrayExtra("all_path");
////            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
//            for (String string : all_path) {
//                Log.e("string:123","image: "+string);
////                CustomGallery item = new CustomGallery();
////                item.sdcardPath = string;
////                dataT.add(item);
//            }
//            viewSwitcher.setDisplayedChild(0);
//            adapter.addAll(dataT);

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            String myfilePath = picturePath;
            Bitmap bm = BitmapFactory.decodeFile(getRealPathFromUri(EnquiryForm.this,selectedImage));
            //Picasso.with(context).load(new File(myfilePath)).fit().into(image_view);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
             IMAGE_PATH  = Base64.encodeToString(b, Base64.DEFAULT);

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(EnquiryForm.this);
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Are you sure you want to add image");
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                   //callService();
                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }

        else if (requestCode == 1 && resultCode == RESULT_OK)
            try {
                if (data != null) {

                    Uri selectedImageUri = data.getData();
                    Log.e("IMAGE_PATH_ImaUri",""+selectedImageUri);
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Uri uri = getImageUri(getApplicationContext(),photo);
                    Log.e("IMAGE_PATH_uri",""+uri);
//               Constant.IMG_PATH = getRealPathFromUri(SettingActivity.this,uri);
                    String IMAGE_PATH_ = getRealPathFromUri(EnquiryForm.this,uri);
                    Log.e("IMAGE_PATH_",""+IMAGE_PATH_);
//                    Picasso.with(context).load(new File(IMAGE_PATH_))
//                            .transform(new CropCircleTransformation()).fit()
//                            .into(profile_pic);
//                i1.setImageBitmap(photo);
//                get_dish_img = encodeImage(photo);

                    Bitmap bm = BitmapFactory.decodeFile(getRealPathFromUri(EnquiryForm.this,uri));
                    //..............Changes.........
//                    Bitmap correctBmp = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //..............Changes.........
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    String IMAGE_PATH  = Base64.encodeToString(b, Base64.DEFAULT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }*/

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            base64ImagesArray = new ArrayList<>();
            imagesArray = new ArrayList<>();
            if (requestCode == 5) {
                //Log.e("dataPluse", "" + data.getClipData().getItemCount());// Get count of image here.
                //Log.e("countPluse", "" + data.getClipData().getItemCount());
                if(data.getClipData() != null) {
                    for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        imagesArray.add(imageUri);
                        Log.d("ImageUri", data.getClipData().getItemAt(i).getUri()+"");


                        InputStream imageStream = null;
                        try {
                            imageStream = getContentResolver().openInputStream(imageUri);
                            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            String encodedImage = encodeImage(selectedImage);
                            base64ImagesArray.add(encodedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    Log.d("ImageArray", imagesArray.toString());
                    Log.d("ImageArrayBase64", base64ImagesArray.size()+"");
                    /*multipleEnquiryFormAdapter = new MultipleEnquiryFormAdapter(this, imagesArray, base64ImagesArray);
                    grdImages.setAdapter(multipleEnquiryFormAdapter);*/
                } else {

                    Uri singleImageUri = data.getData();
                    InputStream imageStream = null;
                    imagesArray.add(singleImageUri);
                    try {
                        imageStream = getContentResolver().openInputStream(singleImageUri);
                        Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        String encodedImage = encodeImage(selectedImage);
                        base64ImagesArray.add(encodedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    //Toast.makeText(context, "Single image", Toast.LENGTH_SHORT).show();
                }
                multipleEnquiryFormAdapter = new MultipleEnquiryFormAdapter(this, imagesArray, base64ImagesArray);
                LinearLayoutManager llm = new LinearLayoutManager(this);
                llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                grdImages.setLayoutManager(llm);
                grdImages.setAdapter(multipleEnquiryFormAdapter);


                /*if (data.getClipData().getItemCount() > 5) {
                    adapter.notifyDataSetChanged();
                    Snackbar snackbar = Snackbar
                            .make(findViewById(R.id.btnSelectImg), "You can not select more than 15 images", Snackbar.LENGTH_LONG)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 5);
                                }
                            });
                    snackbar.setActionTextColor(Color.BLUE);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.RED);
                    snackbar.show();

                } else {
                    imagesUriArrayList.clear();

                    for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                        imagesUriArrayList.add(data.getClipData().getItemAt(i).getUri());
                    }
                    Log.e("SIZE", imagesUriArrayList.size() + "");
                    adapter = new DataAdapter(MainActivity.this, imagesUriArrayList);
                    imageresultRecycletview.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }*/
            }
        }
    }


    private static File getOutputMediaFile(int type) {
        //File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.
                DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        }
        else {
            return null;
        }
        return mediaFile;
    }


    private void back() {
        if(!"".equals(from)) {
            if (from.equals("Help")) {
                Intent intent = new Intent(context,Help.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else if (from.equals("Agro_advisory")) {
                Intent intent = new Intent(context,Agro_Advisary.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(context,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(context,Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed(){
        back();
    }
}
