package com.krishipahal;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MultipleEnquiryFormAdapter extends RecyclerView.Adapter<MultipleEnquiryFormAdapter.CustomViewHolder> {

    Context context;
    ArrayList<Uri> array;
    ArrayList<String> base64ImagesArray;
    CustomViewHolder holder;

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageView closeImage;

        public CustomViewHolder(View view) {
            super(view);
            imageView = (ImageView)view.findViewById(R.id.imageView);
            closeImage = (ImageView)view.findViewById(R.id.closeImage);
        }
    }


    public MultipleEnquiryFormAdapter(Context context, ArrayList<Uri> array, ArrayList<String> base64ImagesArray) {
        this.context = context;
        this.array = array;
        this.base64ImagesArray = base64ImagesArray;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_multiple_enquiry_form, parent, false);
        return new CustomViewHolder(itemView);*/

        View view = LayoutInflater.from
                (parent.getContext()).inflate(R.layout.adapter_multiple_enquiry_form, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        Picasso.with(context)
                .load(array.get(position))
                .fit()
                .into(holder.imageView);

        holder.closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                array.remove(position);
                base64ImagesArray.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (null != array ? array.size() : 0);
    }



    /*LayoutInflater inflater;
    ImageView imageView;
    ImageView closeImage;
    Context context;
    ArrayList<Uri> array;
    ArrayList<String> base64ImagesArray;

    public MultipleEnquiryFormAdapter(Context context, ArrayList<Uri> array, ArrayList<String> base64ImagesArray) {
        this.context = context;
        this.array = array;
        this.base64ImagesArray = base64ImagesArray;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view;
        view = inflater.inflate(R.layout.adapter_multiple_enquiry_form,null);
        imageView = (ImageView)view.findViewById(R.id.imageView);
        closeImage = (ImageView)view.findViewById(R.id.closeImage);
        Picasso.with(context)
                .load(array.get(position))
                .fit()
                .into(imageView);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                array.remove(position);
                base64ImagesArray.remove(position);
                notifyDataSetChanged();
            }
        });

        return view;
    }*/
}
