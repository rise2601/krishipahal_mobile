package com.krishipahal;

import android.content.Context;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

/**
 * Created by abc on 03/07/2018.
 */

class ViewPagerBookAdapter extends PagerAdapter {
    private Context mContext;
    LayoutInflater mLayoutInflater;
    PhotoView image_view;
    String[] page_image;
  TextView  page_no;
    public ViewPagerBookAdapter(Context context, String[] page_image) {
        this.mContext = context;
        this.page_image = page_image;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewpager_bookpages, collection, false);
        image_view = (PhotoView)itemView.findViewById(R.id.image_view);
        page_no = (TextView)itemView.findViewById(R.id.page_no);
//        scaleGestureDetector = new ScaleGestureDetector(mContext,new ScaleListener());
        page_no.setText("Page "+(position+1));
        Log.e("Helllooo123", Webservices_url.IMAGE_BASE_URL+page_image[position]);
        Picasso.with(mContext).load(Webservices_url.IMAGE_BASE_URL+page_image[position]).fit().into(image_view);
        //set img here
        collection.addView(itemView);
        return itemView;
    }


//    private class ScaleListener extends ScaleGestureDetector.
//            SimpleOnScaleGestureListener {
//        @Override
//        public boolean onScale(ScaleGestureDetector detector) {
//            float scaleFactor = detector.getScaleFactor();
//            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
//            matrix.setScale(scaleFactor, scaleFactor);
//            image_view.setImageMatrix(matrix);
//            return true;
//        }
//    }


    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return page_image.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
