package com.krishipahal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MembershipForm extends AppCompatActivity implements AdapterView.OnItemSelectedListener  {

    EditText edt_name,edt_mobile,edt_employeeID,edt_amount,edt_address,edt_village,edt_district,edt_state,edt_email,edt_fathername,edt_pincode;
    EditText edt_userName, edt_fermName, edt_fermAddress, edt_userMobile,
            edt_userLandline, edt_userCity, edt_userState, edt_userEmail,
            edt_userPincode, edt_userAmount, edt_userEmployeeId;
    TextView tv_header;
    Spinner occupationSpinner, companySpinner, membershipFarmerSpinner;
    String formValue = "", paymentBusinessValue = "", paymentFarmerValue = "", amountValue = "";
    String name = "",mobile = "",address = "",village = "",district = "",state = "",email = "",userid = "",father = "",pincode = "", employeeId="";
    private static final String IMAGE_DIRECTORY_NAME = "KrishiPahal";
    Context context;
    Button btn_submit;
    ImageView enquiry_back;
    LinearLayout ll_farmer, ll_business;

    String companyTypeValue, occupationValue;
    String userName = "", userFermName = "", userFermAddress = "", userAmountValue = "",
            userMobile = "", userLandline = "", userCity = "", userEmployeeId = "",
            userState = "", userEmail = "", userPincode = "", spinnerBusinessValue = "",
            spinnerFarmerValue = "", membershipSpinnerValue = "", membershipFarmerSpinnerValue;
    String[] occupationArray, companyTypeArray;

    String[] paymentModeArray, membershipTypeArray;
    RadioGroup radioBusinessPayment, radioFarmerPayment;
    RadioButton radioBusinessYes, radioBusinessNo, radioFarmerYes, radioFarmerNo;
    RadioButton radioBusinessButtonValue, radioFarmerButtonValue;
    Spinner paymentBusinessSpinner, paymentFarmerSpinner, membershipTypeBusinessSpinner;
    LinearLayout ll_businessSpinner, ll_FarmerSpinner;

    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_form);
        context = this;

        tv_header = (TextView) findViewById(R.id.tv_header);
        ll_farmer = (LinearLayout) findViewById(R.id.ll_farmer);
        ll_business = (LinearLayout) findViewById(R.id.ll_business);

        edt_userName = (EditText)findViewById(R.id.edt_userName);
        edt_fermName = (EditText)findViewById(R.id.edt_fermName);
        edt_fermAddress = (EditText)findViewById(R.id.edt_fermAddress);
        edt_userMobile = (EditText)findViewById(R.id.edt_userMobile);
        edt_userLandline = (EditText)findViewById(R.id.edt_userLandline);
        edt_userEmployeeId  = (EditText)findViewById(R.id.edt_userEmployeeId);
        edt_userAmount = (EditText)findViewById(R.id.edt_userAmount);
        edt_userCity = (EditText)findViewById(R.id.edt_userCity);
        edt_userState = (EditText)findViewById(R.id.edt_userState);
        edt_userEmail = (EditText)findViewById(R.id.edt_userEmail);
        edt_userPincode = (EditText)findViewById(R.id.edt_userPincode);
        occupationSpinner = (Spinner)findViewById(R.id.occupationSpinner);
        companySpinner = (Spinner)findViewById(R.id.companySpinner);
        membershipFarmerSpinner = (Spinner)findViewById(R.id.membershipFarmerSpinner);

        radioBusinessPayment = (RadioGroup)findViewById(R.id.radioBusinessPayment);
        radioBusinessYes = (RadioButton)findViewById(R.id.radioBusinessYes);
        radioBusinessNo = (RadioButton)findViewById(R.id.radioBusinessNo);
        paymentBusinessSpinner = (Spinner)findViewById(R.id.paymentBusinessSpinner);
        membershipTypeBusinessSpinner = (Spinner)findViewById(R.id.membershipTypeBusinessSpinner);
        ll_businessSpinner = (LinearLayout) findViewById(R.id.ll_businessSpinner);

        radioFarmerPayment = (RadioGroup)findViewById(R.id.radioFarmerPayment);
        radioFarmerYes = (RadioButton)findViewById(R.id.radioFarmerYes);
        radioFarmerNo = (RadioButton)findViewById(R.id.radioFarmerNo);
        paymentFarmerSpinner = (Spinner)findViewById(R.id.paymentFarmerSpinner);
        ll_FarmerSpinner = (LinearLayout) findViewById(R.id.ll_FarmerSpinner);

        membershipTypeArray = getResources().getStringArray(R.array.membershipType_text);
        ArrayAdapter aaaa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, membershipTypeArray);
        aaaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        membershipTypeBusinessSpinner.setAdapter(aaaa);
        membershipFarmerSpinner.setAdapter(aaaa);

        paymentModeArray = getResources().getStringArray(R.array.payment_mode_text);
        ArrayAdapter aaa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, paymentModeArray);
        aaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentBusinessSpinner.setAdapter(aaa);
        paymentFarmerSpinner.setAdapter(aaa);

        occupationArray = getResources().getStringArray(R.array.occupation_text);
        companyTypeArray = getResources().getStringArray(R.array.company_type_text);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, occupationArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        occupationSpinner.setAdapter(aa);

        ArrayAdapter a = new ArrayAdapter(this, android.R.layout.simple_spinner_item, companyTypeArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        companySpinner.setAdapter(a);
        occupationSpinner.setOnItemSelectedListener(this);
        companySpinner.setOnItemSelectedListener(this);
        paymentBusinessSpinner.setOnItemSelectedListener(this);
        paymentFarmerSpinner.setOnItemSelectedListener(this);
        membershipFarmerSpinner.setOnItemSelectedListener(this);
        membershipTypeBusinessSpinner.setOnItemSelectedListener(this);

        edt_name = (EditText)findViewById(R.id.edt_name);
        edt_mobile = (EditText)findViewById(R.id.edt_mobile);
        edt_address = (EditText)findViewById(R.id.edt_address);
        edt_village = (EditText)findViewById(R.id.edt_village);
        edt_district = (EditText)findViewById(R.id.edt_district);
        edt_state = (EditText)findViewById(R.id.edt_state);
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_fathername = (EditText)findViewById(R.id.edt_fathername);
        edt_pincode = (EditText)findViewById(R.id.edt_pincode);
        edt_amount = (EditText)findViewById(R.id.edt_amount);
        edt_employeeID = (EditText)findViewById(R.id.edt_employeeID);
        btn_submit = (Button)findViewById(R.id.btn_submit);
        enquiry_back = (ImageView) findViewById(R.id.enquiry_back);

        formValue = getIntent().getStringExtra("formValue");
        if(formValue.equals(getResources().getString(R.string.farmer_text))) {
            tv_header.setText("Farmer Form");
            ll_farmer.setVisibility(View.VISIBLE);
            ll_business.setVisibility(View.GONE);
        } else if(formValue.equals(getResources().getString(R.string.businessman_text))) {
            tv_header.setText("Business Form");
            ll_farmer.setVisibility(View.GONE);
            ll_business.setVisibility(View.VISIBLE);
        } else {
            tv_header.setText("Farmer Form");
            ll_farmer.setVisibility(View.VISIBLE);
            ll_business.setVisibility(View.GONE);
        }

        enquiry_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userid = preferences.getString("userid",null);

        radioBusinessPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioBusinessButtonValue = (RadioButton) findViewById(selectedId);
                paymentBusinessValue = radioBusinessButtonValue.getText().toString().trim();
                if(paymentBusinessValue.equals(getResources().getString(R.string.radio_yes_text))) {
                    edt_userAmount.setVisibility(View.VISIBLE);
                    ll_businessSpinner.setVisibility(View.VISIBLE);
                } else if(paymentBusinessValue.equals(getResources().getString(R.string.radio_no_text))) {
                    edt_userAmount.setVisibility(View.GONE);
                    ll_businessSpinner.setVisibility(View.GONE);
                } else {
                    edt_userAmount.setVisibility(View.GONE);
                    ll_businessSpinner.setVisibility(View.GONE);
                }
            }
        });

        radioFarmerPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioFarmerButtonValue = (RadioButton) findViewById(selectedId);
                paymentFarmerValue = radioFarmerButtonValue.getText().toString().trim();
                if(paymentFarmerValue.equals(getResources().getString(R.string.radio_yes_text))) {
                    ll_FarmerSpinner.setVisibility(View.VISIBLE);
                    edt_amount.setVisibility(View.VISIBLE);
                } else if(paymentFarmerValue.equals(getResources().getString(R.string.radio_no_text))) {
                    ll_FarmerSpinner.setVisibility(View.GONE);
                    edt_amount.setVisibility(View.GONE);
                } else {
                    ll_FarmerSpinner.setVisibility(View.GONE);
                    edt_amount.setVisibility(View.GONE);
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(formValue.equals(getResources().getString(R.string.farmer_text))) {

                    name = edt_name.getText().toString();
                    mobile = edt_mobile.getText().toString();
                    address = edt_address.getText().toString();
                    village = edt_village.getText().toString();
                    state = edt_state.getText().toString();
                    email = edt_email.getText().toString();
                    father = edt_fathername.getText().toString();
                    pincode = edt_pincode.getText().toString();
                    employeeId = edt_employeeID.getText().toString().trim();
                    amountValue = edt_amount.getText().toString().trim();

                    if (name.equals("")||mobile.equals("")||address.equals("")||village.equals("")
                            ||state.equals("")||father.equals("") || employeeId.equals("")){
                        Toast.makeText(context,"Fill all details of complusary fields...!!!",Toast.LENGTH_SHORT).show();
                    } else if (mobile.length()<10) {
                        Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if(!"".equals(email)) {
                            if(!isValidEmail(email)) {
                                Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                            } else {
                                try{makepostRequest();}catch (Exception e){e.printStackTrace();}
                            }
                        } else {
                            try{makepostRequest();}catch (Exception e){e.printStackTrace();}
                        }
                    }

                } else if(formValue.equals(getResources().getString(R.string.businessman_text))) {
                    userName = edt_userName.getText().toString().toString().trim();
                    userFermName = edt_fermName.getText().toString().toString().trim();
                    userFermAddress = edt_fermAddress.getText().toString().toString().trim();
                    userMobile = edt_userMobile.getText().toString().toString().trim();
                    userEmployeeId = edt_userEmployeeId.getText().toString().toString().trim();
                    userLandline = edt_userLandline.getText().toString().toString().trim();
                    userCity = edt_userCity.getText().toString().toString().trim();
                    userState = edt_userState.getText().toString().toString().trim();
                    userEmail = edt_userEmail.getText().toString().toString().trim();
                    userPincode = edt_userPincode.getText().toString().toString().trim();
                    userAmountValue = edt_userAmount.getText().toString().trim();

                    if (userName.equals("")||userFermName.equals("")||userFermAddress.equals("")
                            ||userMobile.equals("")||userLandline.equals("")
                            ||userCity.equals("") ||userState.equals("")
                            ||userPincode.equals("") || userEmployeeId.equals("")){
                        Toast.makeText(context,"Fill all details of complusary fields...!!!",Toast.LENGTH_SHORT).show();
                    } else if (userMobile.length()<10) {
                        Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if(!"".equals(email)) {
                            if(!isValidEmail(email)) {
                                Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                            } else {
                                try{makepostRequest();
                                }catch (Exception e){e.printStackTrace();}
                            }
                        } else {
                            try{
                                makepostRequest();
                            }
                            catch (Exception e){e.printStackTrace();}
                        }
                    }
                } else {

                    name = edt_name.getText().toString();
                    mobile = edt_mobile.getText().toString();
                    address = edt_address.getText().toString();
                    village = edt_village.getText().toString();
                    state = edt_state.getText().toString();
                    email = edt_email.getText().toString();
                    father = edt_fathername.getText().toString();
                    pincode = edt_pincode.getText().toString();
                    employeeId = edt_employeeID.getText().toString();
                    amountValue = edt_amount.getText().toString().trim();

                    if (name.equals("")||mobile.equals("")||address.equals("")||village.equals("")
                            ||state.equals("")||father.equals("")||employeeId.equals("")){
                        Toast.makeText(context,"Fill all details of complusary fields...!!!",Toast.LENGTH_SHORT).show();
                    } else if (mobile.length()<10) {
                        Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if(!"".equals(email)) {
                            if(!isValidEmail(email)) {
                                Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                            } else {
                                try{
                                    makepostRequest();
                                }catch (Exception e){e.printStackTrace();}
                            }
                        } else {
                            try{
                                makepostRequest();
                            }catch (Exception e){e.printStackTrace();}
                        }
                    }
                }
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void makepostRequest() {
        StringRequest postRequest = new StringRequest(Request.Method.POST,Webservices_url.Membershippostdata ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            Log.e("response_msg1423",""+jsonResponse.toString());

                            String status = jsonResponse.getString("status");
                            if (status.equals("200")){
                                Toast.makeText(context,"Your request is send successfully...",Toast.LENGTH_LONG).show();
                                hidepDialog();
                                Intent intent =new Intent(context,Home.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else if (status.equals("400")){
                                hidepDialog();
                                Toast.makeText(context,"Number already exists. Please enter another number.",Toast.LENGTH_LONG).show();
                            }
                            else {
                                hidepDialog();
                                Toast.makeText(context,"Something wrong!!! Please try again later...",Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("error_error",""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = null;
                if(formValue.equals(getResources().getString(R.string.farmer_text))) {
                    params = new HashMap<>();
                    params.put("type", ""+"Farmer");
                    params.put("name", ""+name);
                    params.put("mobile", ""+mobile);
                    params.put("address", ""+address);
                    params.put("fathername", ""+father);
                    params.put("phone", ""+mobile);
                    params.put("email", ""+email);
                    params.put("pincode", ""+pincode);
                    if(!paymentFarmerValue.isEmpty() && !"".equals(paymentFarmerValue)) {
                        params.put("payment", ""+paymentFarmerValue);
                    } else {
                        paymentFarmerValue = getResources().getString(R.string.radio_yes_text);
                        params.put("payment", "Yes");
                    }
                    if(paymentFarmerValue.equals(getResources().getString(R.string.radio_yes_text))) {
                        params.put("payment_mode", ""+spinnerFarmerValue);
                        params.put("me_amount", ""+amountValue);
                    } else if(paymentFarmerValue.equals(getResources().getString(R.string.radio_no_text))) {
                        params.put("payment_mode", "");
                        params.put("me_amount", "");
                    } else {
                        params.put("payment_mode", ""+spinnerFarmerValue);
                        params.put("me_amount", ""+amountValue);
                    }
                    params.put("village", ""+village);
                    params.put("userid", ""+userid);
                    params.put("state", ""+state);
                    params.put("employee_id", ""+employeeId);
                    params.put("membership_type", ""+membershipFarmerSpinnerValue);
                    Log.d("typeee", membershipFarmerSpinnerValue);

                } else if(formValue.equals(getResources().getString(R.string.businessman_text))) {
                    params = new HashMap<>();
                    params.put("type", ""+"Buisness");
                    params.put("name", ""+userName);
                    params.put("ferm_name", ""+userFermName);
                    params.put("ferm_address", ""+userFermAddress);
                    if(!paymentBusinessValue.isEmpty() && !"".equals(paymentBusinessValue)) {
                        params.put("payment", ""+paymentBusinessValue);
                    } else {
                        paymentBusinessValue = getResources().getString(R.string.radio_yes_text);
                        params.put("payment", "Yes");
                    }
                    if(paymentBusinessValue.equals(getResources().getString(R.string.radio_yes_text))) {
                        params.put("payment_mode", ""+spinnerBusinessValue);
                        params.put("me_amount", ""+userAmountValue);
                    } else if(paymentBusinessValue.equals(getResources().getString(R.string.radio_no_text))) {
                        params.put("payment_mode", "");
                        params.put("me_amount", "");
                    } else {
                        params.put("payment_mode", ""+spinnerBusinessValue);
                        params.put("me_amount", ""+userAmountValue);
                    }
                    params.put("mobile", ""+userMobile);
                    params.put("phone", ""+userLandline);
                    params.put("city", ""+userCity);
                    params.put("state", ""+userState);
                    params.put("membership_type", ""+membershipSpinnerValue);
                    params.put("employee_id", ""+userEmployeeId);
                    params.put("email", ""+userEmail);
                    params.put("pincode", ""+userPincode);
                    params.put("company_type", ""+companyTypeValue);
                    params.put("userid", ""+userid);
                    params.put("occupation_type", ""+occupationValue);
                } else {
                    params = new HashMap<>();
                    params.put("type", ""+"Farmer");
                    params.put("name", ""+name);
                    params.put("mobile", ""+mobile);
                    params.put("address", ""+address);
                    params.put("fathername", ""+father);
                    params.put("phone", ""+mobile);
                    if(!paymentFarmerValue.isEmpty() && !"".equals(paymentFarmerValue)) {
                        params.put("payment", ""+paymentFarmerValue);
                    } else {
                        paymentFarmerValue = getResources().getString(R.string.radio_yes_text);
                        params.put("payment", "Yes");
                    }
                    if(paymentFarmerValue.equals(getResources().getString(R.string.radio_yes_text))) {
                        params.put("payment_mode", ""+spinnerFarmerValue);
                        params.put("me_amount", ""+amountValue);
                    } else if(paymentFarmerValue.equals(getResources().getString(R.string.radio_no_text))) {
                        params.put("payment_mode", "");
                        params.put("me_amount", "");
                    } else {
                        params.put("payment_mode", ""+spinnerFarmerValue);
                        params.put("me_amount", ""+amountValue);
                    }
                    params.put("email", ""+email);
                    params.put("pincode", ""+pincode);
                    params.put("village", ""+village);
                    params.put("userid", ""+userid);
                    params.put("state", ""+state);
                    params.put("employee_id", ""+employeeId);
                    params.put("membership_type", ""+membershipFarmerSpinnerValue);
                }
                Log.d("parramsssssssss", params+"");
                return params;
            }

        };
        int socketTimeout = 90000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(postRequest);
        AppController.getInstance().getRequestQueue().getCache().clear();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();}

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();}

    private void back(){
        Intent intent = new Intent(context,MembershipActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Spinner spinner = (Spinner) adapterView;
        if(spinner.getId() == R.id.occupationSpinner) {
            occupationValue = occupationArray[i];
        } else if(spinner.getId() == R.id.companySpinner) {
            companyTypeValue = companyTypeArray[i];
        } else if(spinner.getId() == R.id.paymentBusinessSpinner) {
            spinnerBusinessValue = paymentModeArray[i];
        } else if(spinner.getId() == R.id.paymentFarmerSpinner) {
            spinnerFarmerValue = paymentModeArray[i];
        } else if(spinner.getId() == R.id.membershipTypeBusinessSpinner) {
            membershipSpinnerValue = membershipTypeArray[i];
        } else if(spinner.getId() == R.id.membershipFarmerSpinner) {
            membershipFarmerSpinnerValue = membershipTypeArray[i];
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
