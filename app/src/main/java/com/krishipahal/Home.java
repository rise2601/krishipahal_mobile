package com.krishipahal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends AppCompatActivity {
    NavigationView navigationView;
    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    View headerview;
    private Boolean exit = false;
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences.Editor preferences;
    Context context;
    TextView tv_userid;
    LinearLayout nav_home, nav_gallary, nav_profile, nav_invite, nav_about, nav_contactus, nav_logout, nav_vision, nav_career,
            nav_kitchen,nav_advisary,nav_terrace, newsUpdate;
    LinearLayout ebook,agro_adv,agri_tech,membership,mandibhav,gallery;

    private void initialisation() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        tv_userid = (TextView) headerview.findViewById(R.id.tv_userid);
        nav_home = (LinearLayout) headerview.findViewById(R.id.nav_home);
        nav_gallary = (LinearLayout) headerview.findViewById(R.id.nav_gallary);
        nav_profile = (LinearLayout) headerview.findViewById(R.id.nav_profile);
        nav_invite = (LinearLayout) headerview.findViewById(R.id.nav_invite);
        nav_about = (LinearLayout) headerview.findViewById(R.id.nav_about);
        nav_contactus = (LinearLayout) headerview.findViewById(R.id.nav_contactus);
        nav_logout = (LinearLayout) headerview.findViewById(R.id.nav_logout);
        nav_vision = (LinearLayout) headerview.findViewById(R.id.nav_vision);
        nav_career = (LinearLayout) headerview.findViewById(R.id.nav_career);
        nav_terrace = (LinearLayout) headerview.findViewById(R.id.nav_terrace);
        nav_advisary = (LinearLayout) headerview.findViewById(R.id.nav_advisary);
        nav_kitchen = (LinearLayout) headerview.findViewById(R.id.nav_kitchen);
        ebook = (LinearLayout)findViewById(R.id.ebook);
        agro_adv = (LinearLayout)findViewById(R.id.agro_adv);
        agri_tech = (LinearLayout)findViewById(R.id.agri_tech);
        membership = (LinearLayout)findViewById(R.id.membership);
        mandibhav = (LinearLayout)findViewById(R.id.mandibhav);
        newsUpdate = (LinearLayout)findViewById(R.id.newsUpdate);
        gallery = (LinearLayout)findViewById(R.id.gallery);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        tv_userid.setText(getResources().getString(R.string.user_id_text) + ": 4590");
    }

    private void listiners() {
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
            }
        });
        ebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,Book_grid.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        agro_adv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,Agro_Advisary.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        agri_tech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,Help.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,MembershipActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        mandibhav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Comming soon...!!", Toast.LENGTH_LONG).show();
            }
        });
        newsUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Comming soon...!!", Toast.LENGTH_LONG).show();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,GalleryTitle.class);
                //Intent intent = new Intent(Home.this,Gallery.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, Invite_n_earn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, About_Us.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, ContactUs.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(Home.this,GalleryTitle.class);
                //Intent intent = new Intent(context, Gallery.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, KitchenGarden.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_terrace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, TerraceGarden.class);
                intent.putExtra("from_where","0");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_advisary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, Agro_Advisary.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_vision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, Ourvision.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_career.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                Intent intent = new Intent(context, Career.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        nav_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_navigation();
                preferences.clear();
                preferences.commit();
                Intent intent = new Intent(context, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void check_navigation() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        initialisation();
        listiners();
    }

    @Override
    public void onBackPressed() {
        if (exit.equals(true)) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
                super.finish();
            } else {
                super.finish();
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }
}

