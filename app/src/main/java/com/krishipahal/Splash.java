package com.krishipahal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.Locale;

public class Splash extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public static final String MY_PREFS_NAME = "Krishipahal";
    private static int SPLASH_TIME_OUT = 3000;
    Context context;
    String user_id = "";
    String check_otp = "";
    SharedPreferences preferences;
    String langValue;
    String[] languageArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        user_id = preferences.getString("userid",null);
        languageArray = getResources().getStringArray(R.array.laguage_text);
        languageAlert();

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(preferences.contains("userid")){

                    Intent i = new Intent(Splash.this, Home.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(i);

                }
                else {
                    Intent i = new Intent(Splash.this, Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(i);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);*/
    }

    private void languageAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
        builder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.laguage_dialog,null);
        builder.setView(dialogView);
        Spinner spinner = (Spinner) dialogView.findViewById(R.id.spinner);
        Button ok_btn = (Button) dialogView.findViewById(R.id.ok_btn);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, languageArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);

        final AlertDialog dialog = builder.create();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(langValue.equals(getResources().getString(R.string.hindi_text))) {
                    setLocale("hi");
                } else if(langValue.equals(getResources().getString(R.string.english_text))) {
                    setLocale("en");
                } else {
                    setLocale("en");
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        langValue = languageArray[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        if(preferences.contains("userid")){
            Intent i = new Intent(this, Home.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(this, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
}
