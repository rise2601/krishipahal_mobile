package com.krishipahal;
public class GalleryPicsModel {
    String gallery_id, title, imagename, image_size, image_type, image_url;
    public String getGalleryId() {
        return gallery_id;
    }
    public void setGalleryId(String gallery_id) {
        this.gallery_id = gallery_id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getImageName() {
        return imagename;
    }
    public void setImageName(String imagename) {
        this.imagename = imagename;
    }
    public String getImageSize() {
        return image_size;
    }
    public void setImageSize(String image_size) {
        this.image_size = image_size;
    }
    public String getImageType() {
        return image_type;
    }
    public void setImageType(String image_type) {
        this.image_type = image_type;
    }
    public String getImageUrl() {
        return image_url;
    }
    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }
}
