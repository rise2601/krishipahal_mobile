package com.krishipahal;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<GalleryPicsModel> galleryPicsList;
    public GalleryAdapter(Context context, ArrayList<GalleryPicsModel> galleryPicsList) {
        this.context = context;
        this.galleryPicsList = galleryPicsList;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image_gallery;
        public MyViewHolder(View view) {
            super(view);
            image_gallery = (ImageView)view.findViewById(R.id.image_gallery);
        }
    }
    @Override
    public GalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.galleryitemview, parent, false);
        return new GalleryAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(GalleryAdapter.MyViewHolder holder, int position) {
        final GalleryPicsModel galleryPicsModel = galleryPicsList.get(position);
        Picasso.with(context).load(Webservices_url.gallery_image+galleryPicsModel.getImageName()).fit().into(holder.image_gallery);
        holder.image_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZoomGalleryImage.class);
                intent.putExtra("IMAGE", galleryPicsModel.getImageName());
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return galleryPicsList.size();
    }
}




/*class GalleryAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ImageView image_gallery;
    ArrayList<GalleryPicsModel> galleryPicsList;
    public GalleryAdapter(Context context, ArrayList<GalleryPicsModel> galleryPicsList) {
        this.context = context;
        this.galleryPicsList = galleryPicsList;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return galleryPicsList.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        view = inflater.inflate(R.layout.galleryitemview,null);
        final GalleryPicsModel galleryPicsModel = galleryPicsList.get(position);
        image_gallery = (ImageView)view.findViewById(R.id.image_gallery);
        Picasso.with(context).load(Webservices_url.gallery_image+galleryPicsModel.getImageName()).fit().into(image_gallery);
        image_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZoomGalleryImage.class);
                intent.putExtra("IMAGE", galleryPicsModel.getImageName());
                context.startActivity(intent);
            }
        });
        return view;
    }
}*/
