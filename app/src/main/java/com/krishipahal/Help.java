package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Help extends AppCompatActivity {

    Context context;
    ImageView career_back;
    LinearLayout ll_net,ll_poly_house,ll_green_house,ll_machi,ll_crop,ll_horti,ll_garden,ll_pashu_palan;

    void initialisation(){
        ll_net = (LinearLayout)findViewById(R.id.ll_net);
        ll_poly_house = (LinearLayout)findViewById(R.id.ll_poly_house);
        ll_green_house = (LinearLayout)findViewById(R.id.ll_green_house);
        ll_machi = (LinearLayout)findViewById(R.id.ll_machi);
        ll_crop = (LinearLayout)findViewById(R.id.ll_crop);
        ll_horti = (LinearLayout)findViewById(R.id.ll_horti);
        ll_garden = (LinearLayout)findViewById(R.id.ll_garden);
        ll_pashu_palan = (LinearLayout)findViewById(R.id.ll_pashu_palan);
        career_back = (ImageView)findViewById(R.id.career_back);
    }

    void clicklisteners(){
        ll_net.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TerraceGarden.class);
                intent.putExtra("from_where","1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_poly_house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TerraceGarden.class);
                intent.putExtra("from_where","2");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_green_house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TerraceGarden.class);
                intent.putExtra("from_where","3");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_machi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TerraceGarden.class);
                intent.putExtra("from_where","4");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        ll_crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","crop");
                intent.putExtra("From","Help");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_horti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","horticulture");
                intent.putExtra("From","Help");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_garden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","garden");
                intent.putExtra("From","Help");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        ll_pashu_palan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","pahupalan");
                intent.putExtra("From","Help");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        career_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });
    }

    private void back(){
        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        context = this;
        initialisation();
        clicklisteners();
    }
}
