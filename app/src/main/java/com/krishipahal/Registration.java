package com.krishipahal;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by abc on 26/06/2018.
 */
public class Registration extends AppCompatActivity {
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences.Editor preferences;
    Context context;
    TextView txt_alrdy;
    Button button;
    String url = "";
    EditText edt_name,edt_mobile,edt_password, edt_referalCode;
    EditText edt_address,edt_pincode, edt_fathername, edt_state, edt_email;

    String s_name = "",s_mobile = "",s_password = "", referalCodeValue = "";
    String addressValue = "", pincodeValue = "", fatherValue = "", stateValue = "", mailValue = "";

    private void initialisation(){

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

        edt_name = (EditText)findViewById(R.id.edt_name);
        edt_mobile = (EditText)findViewById(R.id.edt_mobile);
        edt_password = (EditText)findViewById(R.id.edt_password);
        txt_alrdy = (TextView)findViewById(R.id.txt_alrdy);
        button = (Button)findViewById(R.id.button);
        edt_address = (EditText)findViewById(R.id.edt_address);
        edt_pincode = (EditText)findViewById(R.id.edt_pincode);
        edt_fathername = (EditText)findViewById(R.id.edt_fathername);
        edt_state = (EditText)findViewById(R.id.edt_state);
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_referalCode = (EditText)findViewById(R.id.edt_referalCode);
    }

    private void listiners(){

        txt_alrdy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_name = edt_name.getText().toString();
                s_mobile = edt_mobile.getText().toString();
                s_password = edt_password.getText().toString();
                addressValue = edt_address.getText().toString();
                pincodeValue = edt_pincode.getText().toString();
                fatherValue = edt_fathername.getText().toString();
                stateValue = edt_state.getText().toString();
                mailValue = edt_email.getText().toString();
                referalCodeValue = edt_referalCode.getText().toString().trim();

                if (s_name.equals("")||s_password.equals("")||s_mobile.equals("")||s_mobile.length()<10
                || addressValue.equals("") || pincodeValue.equals("") || fatherValue.equals("")
                        || stateValue.equals("") || referalCodeValue.equals(""))
                {
                    if (s_mobile.length()<10) {
                        Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, "Please fill all entries...", Toast.LENGTH_SHORT).show();
                    }
                }
                else {

                    if(!"".equals(mailValue)) {
                        if(!isValidEmail(mailValue)) {
                            Toast.makeText(context, "Please enter valid email...", Toast.LENGTH_SHORT).show();
                        } else {
                            if(!referalCodeValue.equals("")) {
                                if(referalCodeValue.length()==4) {
                                    url = "http://krishipahal.com/Apiservices/AddUser?username="+s_name+"&password="+s_password+"&mobile="+s_mobile
                                            +"&address="+addressValue+"&pincode="+pincodeValue+"&father_name="+fatherValue+"&state="+stateValue
                                            +"&email="+mailValue+"&referal="+referalCodeValue;
                                    make_registration_request();
                                } else {
                                    Toast.makeText(context, "Please enter 4 digit referral code", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                url = "http://krishipahal.com/Apiservices/AddUser?username="+s_name+"&password="+s_password+"&mobile="+s_mobile
                                        +"&address="+addressValue+"&pincode="+pincodeValue+"&father_name="+fatherValue+"&state="+stateValue
                                        +"&email="+mailValue+"&referal="+referalCodeValue;
                                make_registration_request();
                            }
                        }
                    } else {
                        if(!referalCodeValue.equals("")) {
                            if(referalCodeValue.length()==4) {
                                url = "http://krishipahal.com/Apiservices/AddUser?username="+s_name+"&password="+s_password+"&mobile="+s_mobile
                                        +"&address="+addressValue+"&pincode="+pincodeValue+"&father_name="+fatherValue+"&state="+stateValue
                                        +"&email="+mailValue+"&referal="+referalCodeValue;
                                make_registration_request();
                            } else {
                                Toast.makeText(context, "Please enter 4 digit referral code", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            url = "http://krishipahal.com/Apiservices/AddUser?username="+s_name+"&password="+s_password+"&mobile="+s_mobile
                                    +"&address="+addressValue+"&pincode="+pincodeValue+"&father_name="+fatherValue+"&state="+stateValue
                                    +"&email="+mailValue+"&referal="+referalCodeValue;
                            make_registration_request();
                        }
                        /*url = "http://krishipahal.com/Apiservices/AddUser?username="+s_name+"&password="+s_password+"&mobile="+s_mobile
                                +"&address="+addressValue+"&pincode="+pincodeValue+"&father_name="+fatherValue+"&state="+stateValue
                                +"&email="+mailValue+"&referal="+referalCodeValue;
                        make_registration_request();*/
                    }
                }
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        context  = this;
        initialisation();
        listiners();
    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void make_registration_request() {
        showpDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,url
                , null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("200")){

                        JSONObject userdetails = jsonObject.getJSONObject("userdetails");
                        String username = userdetails.getString("username");
                        String mobile = userdetails.getString("mobile");
                        String userid = jsonObject.getString("userid");

                        preferences.putString("username",""+username);
                        preferences.putString("mobile",""+mobile);
                        preferences.putString("userid",""+userid);
                        preferences.commit();
                        Intent intent = new Intent(context,Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }

                    else{
                        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
                    }

                }
                catch (Exception e){
                    e.printStackTrace();
                }
                hidepDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                hidepDialog();
            }
        });
        AppController.getInstance().getRequestQueue().getCache().invalidate(""+url, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }
}
