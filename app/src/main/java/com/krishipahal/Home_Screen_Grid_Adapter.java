package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by abc on 27/06/2018.
 */

class Home_Screen_Grid_Adapter extends BaseAdapter {

    LayoutInflater inflater;

    ImageView grid_img;
    TextView grid_txt_name;
    TextView txt_date;

    Context context;
    String[] book_id, book_name, book_cover, created_date, created_by, book_url_file;


//    public Home_Screen_Grid_Adapter(Context context) {
//
//        this.context = context;
//        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }

    public Home_Screen_Grid_Adapter(Context context, String[] book_id, String[] book_name, String[] book_cover,
                                    String[] created_date, String[] created_by, String[] book_url_file) {

        this.context = context;
        this.book_id = book_id;
        this.book_name = book_name;
        this.book_cover = book_cover;
        this.created_date = created_date;
        this.created_by = created_by;
        this.book_url_file = book_url_file;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return book_id.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view;
        view = inflater.inflate(R.layout.home_grid_items,null);
        grid_img = (ImageView)view.findViewById(R.id.grid_img);
        grid_txt_name = (TextView)view.findViewById(R.id.grid_txt_name);
        txt_date = (TextView)view.findViewById(R.id.txt_date);
        String [] date = created_date[position].split(" ");
        txt_date.setText(date[0]);
        grid_txt_name.setText(""+book_name[position]);
        Picasso.with(context).load(Webservices_url.IMAGE_BASE_URL+book_cover[position]).fit().into(grid_img);

        grid_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,OpenPdf.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("bookid",""+book_id[position]);
                intent.putExtra("bookname",""+book_name[position]);
                context.startActivity(intent);
            }
        });

        return view;
    }
}
