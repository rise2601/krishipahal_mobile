package com.krishipahal;

/**
 * Created by Silky Sinha on 27/06/2018.
 */

class Webservices_url {

    public static String REGISTRATION_URL = "";
    public static String IMAGE_BASE_URL = "http://krishipahal.com/uploads/";
    public static String booklist = "http://krishipahal.com/Apiservices/BookListapi";
    public static String gallerylist = "http://krishipahal.com/Apiservices/gallerylist";
    public static String gallery_image = "http://krishipahal.com/uploads/gallery/";
    public static String Agroadvisorypost = "http://krishipahal.com/Apiservices/Agroadvisorypost?";
    public static String Membershippostdata = "http://krishipahal.com/Apiservices/Membershippostdata";
    public static String bookpages = "http://krishipahal.com/Apiservices/Singlebook/";

}
