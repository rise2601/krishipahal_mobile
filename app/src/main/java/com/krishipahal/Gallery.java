package com.krishipahal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class Gallery extends AppCompatActivity {

    GridView gallery_grid;
    Context context;
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    //SharedPreferences preferences;
    String [] gallery_id;
    String [] title;
    String [] imagename;
    String [] image_size;
    String [] image_type;
    String [] image_url;
    ImageView gallery_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        context = this;
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        //preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        gallery_grid = (GridView)findViewById(R.id.gallery_grid);
        gallery_back = (ImageView)findViewById(R.id.gallery_back);

//      gallery_grid.setAdapter(new GalleryAdapter(context));
        gallery_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        try {
            //make_gallery_request();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void back() {
        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    @Override
    public void onBackPressed(){
        back();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /*private void make_gallery_request() {
        showpDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,Webservices_url.gallerylist
                , null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Helllooo123", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    Log.e("jsonObjectttttt",jsonObject+"");
                    JSONObject response12 = jsonObject.getJSONObject("response");
                    String status = response12.getString("status");
                    JSONArray data = response12.getJSONArray("data");
                    gallery_id = new String[data.length()];
                    title = new String[data.length()];
                    imagename = new String[data.length()];
                    image_size = new String[data.length()];
                    image_type = new String[data.length()];
                    image_url = new String[data.length()];

                    for (int i = 0; i<data.length();i++){
                        JSONObject object = data.getJSONObject(i);
                        gallery_id [i]= object.getString("gallery_id");
                        title [i] = object.getString("title");
                        imagename[i] = object.getString("imagename");
                        image_size [i] = object.getString("image_size");
                        image_type [i] = object.getString("image_type");
                        image_url [i]= object.getString("image_url");
                    }
   gallery_grid.setAdapter(new GalleryAdapter(context,gallery_id,title,imagename,image_size,image_type,image_url));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                hidepDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                hidepDialog();
            }
        });
        AppController.getInstance().getRequestQueue().getCache().invalidate(Webservices_url.booklist, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }*/
}
