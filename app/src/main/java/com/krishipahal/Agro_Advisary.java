package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Agro_Advisary extends AppCompatActivity {

    Button kitchenGarden, terraceGarden, horticulture, pashuPalan, crop, machinery;
    Context context;
    ImageView back_agro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agro_advisory);
        context = this;
        kitchenGarden = (Button)findViewById(R.id.kitchenGarden);
        terraceGarden = (Button)findViewById(R.id.terraceGarden);
        horticulture = (Button)findViewById(R.id.horticulture);
        pashuPalan = (Button)findViewById(R.id.pashuPalan);
        crop = (Button)findViewById(R.id.crop);
        machinery = (Button)findViewById(R.id.machinery);
        back_agro = (ImageView)findViewById(R.id.back_agro);

        back_agro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        kitchenGarden.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(context,EnquiryForm.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               intent.putExtra("agro_type","KitchenGarden");
               intent.putExtra("From","Agro_advisory");
               startActivity(intent);
           }
       });
        terraceGarden.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(context,EnquiryForm.class);
               intent.putExtra("agro_type","TerraceGarden");
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               intent.putExtra("From","Agro_advisory");
               startActivity(intent);
           }
       });
        horticulture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("agro_type","Horticulture");
                intent.putExtra("From","Agro_advisory");
                startActivity(intent);
            }
        });
        pashuPalan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","PashuPalan");
                intent.putExtra("From","Agro_advisory");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","Crop");
                intent.putExtra("From","Agro_advisory");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        machinery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EnquiryForm.class);
                intent.putExtra("agro_type","Machinery");
                intent.putExtra("From","Agro_advisory");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void back(){
        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }

}
