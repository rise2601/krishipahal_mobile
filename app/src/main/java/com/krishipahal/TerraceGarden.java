package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class TerraceGarden extends AppCompatActivity {
    Context context;
    ImageView terrace_back;
    TextView txt_num,txt_email;
    String get_from = "0";
    TextView text_tittle,content_details;
    ImageView image_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terrace_garden);
        context = this;
        terrace_back = (ImageView)findViewById(R.id.terrace_back);
        image_view = (ImageView)findViewById(R.id.image_view);
        txt_email = (TextView)findViewById(R.id.txt_email);
        txt_num = (TextView)findViewById(R.id.txt_num);
        text_tittle = (TextView)findViewById(R.id.text_tittle);
        content_details = (TextView)findViewById(R.id.content_details);

        get_from = getIntent().getStringExtra("from_where");

        if (get_from.equals("1")){
            text_tittle.setText(getResources().getString(R.string.net_house_text));
            content_details.setText(getResources().getString(R.string.net_content_text));
            Picasso.with(context).load(R.drawable.net_house_image).fit().centerCrop().into(image_view);


        }
        else if (get_from.equals("2")){
            text_tittle.setText(getResources().getString(R.string.poly_house_text));
            content_details.setText(getResources().getString(R.string.poly_house_content_text));
            Picasso.with(context).load(R.drawable.polyhouse).fit().centerCrop().into(image_view);

        }
        else if (get_from.equals("3")){

            text_tittle.setText(getResources().getString(R.string.green_house_text));
            content_details.setText(getResources().getString(R.string.green_house_content_text));
            Picasso.with(context).load(R.drawable.greenhouse_img).fit().centerCrop().into(image_view);

        }
        else if (get_from.equals("4")){

            text_tittle.setText(getResources().getString(R.string.machinery_text));
            content_details.setText(getResources().getString(R.string.machinery_content_text));
            Picasso.with(context).load(R.drawable.machins_images).fit().centerCrop().into(image_view);

        }
        else if (get_from.equals("0")){

        }

        terrace_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        txt_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "+917869311456"));
                startActivity(intent);
            }
        });


        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.setPackage("com.google.android.gm");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"krishipahal@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "For Query");
                i.putExtra(Intent.EXTRA_TEXT   , "");

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, getResources().getString(R.string.no_email_text), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void back(){
        Intent intent = new Intent(context,Help.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }

}
