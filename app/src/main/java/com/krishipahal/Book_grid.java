package com.krishipahal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class Book_grid extends AppCompatActivity {

    String [] book_id;
    String [] book_name;
    String [] book_month;
    String [] book_pdf_file;
    String [] book_size;
    String [] book_type;
    String [] book_url;
    String [] book_url_file;
    String [] uploaded_month;
    String [] created_date;
    String [] created_by;
    String [] book_cover;
    String [] status1;
    GridView grid_mag;
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences.Editor preferences;
    Context context;
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_grid);
        context = this;
        grid_mag = (GridView) findViewById(R.id.grid_mag);
        back_btn = (ImageView)findViewById(R.id.back_btn);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        make_registration_request();
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Book_grid.this,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void make_registration_request() {
        showpDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,Webservices_url.booklist
                , null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Helllooo123", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    JSONObject response12 = jsonObject.getJSONObject("response");
                    String status = response12.getString("status");
                    JSONArray data = response12.getJSONArray("data");
                    book_id = new String[data.length()];
                    book_name = new String[data.length()];
                    book_month = new String[data.length()];
                    book_pdf_file = new String[data.length()];
                    book_size = new String[data.length()];
                    book_type = new String[data.length()];
                    book_url = new String[data.length()];
                    book_url_file = new String[data.length()];
                    uploaded_month = new String[data.length()];
                    book_cover = new String[data.length()];
                    created_date = new String[data.length()];
                    created_by = new String[data.length()];
                    status1 = new String[data.length()];
                    for (int i = 0; i<data.length();i++){
                        JSONObject object = data.getJSONObject(i);
                        book_id [i]= object.getString("book_id");
                        book_name [i] = object.getString("book_name");
                        book_month[i] = object.getString("book_month");
                        book_pdf_file [i] = object.getString("book_pdf_file");
                        book_size [i] = object.getString("book_size");
                        book_type [i]= object.getString("book_type");
                        book_url [i]= object.getString("book_url");
                        book_url_file [i] = object.getString("book_url_file");
                        uploaded_month [i]= object.getString("uploaded_month");
                        created_date [i]= object.getString("created_date");
                        created_by [i]= object.getString("created_by");
                        status1 [i]= object.getString("status");
                        book_cover [i]= object.getString("book_cover");
                    }
                    grid_mag.setAdapter(new Home_Screen_Grid_Adapter(context,book_id,book_name,book_cover,created_date,created_by,book_url_file));
                }

                catch (Exception e){
                    e.printStackTrace();
                }
                hidepDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                hidepDialog();
            }
        });
        AppController.getInstance().getRequestQueue().getCache().invalidate(Webservices_url.booklist, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }

}
