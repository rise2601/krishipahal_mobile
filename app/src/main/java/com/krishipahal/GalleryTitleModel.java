package com.krishipahal;
import org.json.JSONArray;

import java.util.ArrayList;

public class GalleryTitleModel {
    String id, title, status;
    ArrayList<GalleryPicsModel> galleryPics;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public ArrayList<GalleryPicsModel> getGalleryPics() {
        return galleryPics;
    }
    public void setGalleryPics(ArrayList<GalleryPicsModel> galleryPics) {
        this.galleryPics = galleryPics;
    }
}
