package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class KitchenGarden extends AppCompatActivity {

    Context context;
    ImageView kitchen_back;
    TextView txt_num,txt_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen_garden);
        context = this;
        kitchen_back = (ImageView)findViewById(R.id.kitchen_back);
        txt_email = (TextView)findViewById(R.id.txt_email);
        txt_num = (TextView)findViewById(R.id.txt_num);

        kitchen_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        txt_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                // Send phone number to intent as data
                intent.setData(Uri.parse("tel:" + "++917869311456"));
                // Start the dialer app activity with number
                startActivity(intent);
            }
        });


        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.setPackage("com.google.android.gm");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"krishipahal@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "For Query");
                i.putExtra(Intent.EXTRA_TEXT   , "");

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void back(){
        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

        back();

    }
}
