package com.krishipahal;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
public class GalleryTitle extends AppCompatActivity {
    RecyclerView recycleView;
    ImageView gallery_back;
    private ProgressDialog pDialog;
    ArrayList<GalleryTitleModel> galleryTitleArrayList;
    ArrayList<GalleryPicsModel> galleryPicsArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_title);
        init();
    }
    private void init() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        galleryTitleArrayList = new ArrayList<>();
        recycleView = (RecyclerView) findViewById(R.id.recycleView);
        gallery_back = (ImageView)findViewById(R.id.gallery_back);
        gallery_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GalleryTitle.this,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        galleryTitleRequest();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void galleryTitleRequest() {
        showDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,Webservices_url.gallerylist
                , null, new Response.Listener<JSONObject>(){
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    Log.e("jsonObjectttttt",jsonObject+"");
                    JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                    JSONArray jObj = jsonObject1.getJSONArray("data");
                    for (int i = 0; i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        GalleryTitleModel galleryTitleModel = new GalleryTitleModel();
                        galleryTitleModel.setId(object.getString("id"));
                        galleryTitleModel.setTitle(object.getString("title_name"));
                        galleryTitleModel.setStatus(object.getString("status"));
                        //galleryTitleModel.setGalleryPics(object.getJSONArray("gall"));
                        //JSONArray picsJsonArray = new JSONArray(object.get("gall"));
                        galleryPicsArrayList = new ArrayList<>();
                        JSONArray picsJsonArray = object.getJSONArray("gall");
                        for (int j=0; j<picsJsonArray.length(); j++) {
                            JSONObject picsObject = picsJsonArray.getJSONObject(j);
                            GalleryPicsModel galleryPicsModel = new GalleryPicsModel();
                            galleryPicsModel.setImageUrl(picsObject.getString("image_url"));
                            galleryPicsModel.setImageName(picsObject.getString("imagename"));
                            galleryPicsArrayList.add(galleryPicsModel);
                        }
                        galleryTitleModel.setGalleryPics(galleryPicsArrayList);
                        galleryTitleArrayList.add(galleryTitleModel);
                    }
                    setAdapter();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                hideDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                hideDialog();
            }
        });
        AppController.getInstance().getRequestQueue().getCache().invalidate(Webservices_url.booklist, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }
    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(new GalleryTitleAdapter(this, galleryTitleArrayList));
    }
}
