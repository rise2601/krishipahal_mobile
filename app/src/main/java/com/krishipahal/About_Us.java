package com.krishipahal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class About_Us extends AppCompatActivity {

    ImageView about_back;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about__us);
        about_back = (ImageView)findViewById(R.id.about_back);
        context = this;

        String e = "raghav";
        String s = new String("raghav");
        Log.d("output111111", e.equals(s)+"");
        Log.d("output222222", (e==s)+"");
        Log.d("output333333", e);
        Log.d("output444444", s);

        about_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void back() {
        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }
}
