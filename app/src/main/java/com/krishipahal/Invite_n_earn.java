package com.krishipahal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

/**
 * Created by Silky Sinha on 26/06/2018.
 */

public class Invite_n_earn extends AppCompatActivity {

    ImageView image;
    LinearLayout whatsapp,facebook,more;
    Context context;

    Button invite_contact;
ImageView invite_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_n_earn);
        context = this;
        image = (ImageView)findViewById(R.id.image);
        whatsapp = (LinearLayout)findViewById(R.id.whatsapp);
        facebook = (LinearLayout)findViewById(R.id.facebook);
        more = (LinearLayout)findViewById(R.id.more);
        invite_contact = (Button)findViewById(R.id.invite_contact);
        invite_back = (ImageView) findViewById(R.id.invite_back);

        Picasso.with(this).load(R.drawable.share_n_earnbg).centerCrop().fit().into(image);

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                String shareBodyText ="Download Krishi Pahal App Click on The URL below\n\n"
                        + "https://play.google.com/store/apps/details?id=com.krishipahal";
                intent.putExtra(Intent.EXTRA_SUBJECT, "Shine Brand Seed");
                intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(intent, "Choose sharing method"));
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBodyText ="Download Krishi Pahal App Click on The URL below\n\n"
                        + "https://play.google.com/store/apps/details?id=com.krishipahal";
                intent.putExtra(Intent.EXTRA_SUBJECT, "Shine Brand Seed");
                intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(intent, "Choose sharing method"));
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBodyText ="Download Krishi Pahal App Click on The URL below\n\n"
                        + "https://play.google.com/store/apps/details?id=com.krishipahal";
                intent.putExtra(Intent.EXTRA_SUBJECT, "Shine Brand Seed");
                intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(intent, "Choose sharing method"));
            }
        });

        invite_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri uri = Uri.parse("smsto:"+"");
                    Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
                    smsIntent.putExtra("sms_body", "Download Krishi Pahal App Click on The URL below\n\n"
                            + "https://play.google.com/store/apps/details?id=com.krishipahal");
                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        invite_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                krishipahal.com/Apiservices/AddUser?username=silky&password=132456&mobile=7898594663
                startActivity(intent);
            }
        });


    }

    private void back(){

        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

        back();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invite_nearn_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = "Check it out. Your message goes here";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Shearing Option"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
