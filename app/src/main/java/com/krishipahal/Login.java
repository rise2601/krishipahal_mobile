package com.krishipahal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by abc on 26/06/2018.
 */

/*
KRISHIPAHAL@GMAIL.COM
PASS 8462008091*/

public class Login extends AppCompatActivity {

    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences.Editor preferences;
    EditText edt_mobile,edt_password;
    TextView txt_register;
    Button button_login;
    Context context;
    String url = "";
    String mobile = "";
    String pswd = "";
    private Boolean exit = false;

    private void initialisation(){
        edt_mobile = (EditText)findViewById(R.id.edt_mobile);
        edt_password = (EditText)findViewById(R.id.edt_password);
        txt_register = (TextView) findViewById(R.id.txt_register);
        button_login = (Button)findViewById(R.id.button_login);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        preferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
    }


    @Override
    public void onBackPressed() {
        if (exit.equals(true)) {
            super.finish();
        }
        else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    private void listiners(){

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Registration.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile = edt_mobile.getText().toString();
                pswd = edt_password.getText().toString();
                Log.e("mobile_number",""+mobile.length());
                if (mobile.equals("")||pswd.equals("")||mobile.length()<10) {
                    if (mobile.length()<10){
                        Toast.makeText(context, "Enter your 10 digit mobile number...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, "Please fill all entries...", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Log.e("mobile_number",""+mobile.length());
                    url = "http://krishipahal.com/Apiservices/Userlogin?mobile="+mobile+"&password="+pswd;
                   make_login_request();
                    Log.e("mobile_number",""+mobile.length());
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        initialisation();
        listiners();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void make_login_request() {
        showpDialog();
        Log.e("mobile_number",""+mobile.length());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
              ""+url, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Helllooo123", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("200")){
                        JSONObject sessiondata = jsonObject.getJSONObject("sessiondata");
                        String userid = sessiondata.getString("userid");
                        String mobile = sessiondata.getString("mobile");
                        String username = sessiondata.getString("username");
                        preferences.putString("username",""+username);
                        preferences.putString("mobile",""+mobile);
                        preferences.putString("userid",""+userid);
                        preferences.commit();
                        Intent intent = new Intent(context,Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                hidepDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                // Toast.makeText(getApplicationContext(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                hidepDialog();
            }
        });

        AppController.getInstance().getRequestQueue().getCache().invalidate(url, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }
}

