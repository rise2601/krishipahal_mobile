package com.krishipahal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by abc on 26/06/2018.
 */

public class ContactUs extends AppCompatActivity {
    Context context;
    TextView txt_email,txt_num;
    ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        context = this;
        txt_num = (TextView)findViewById(R.id.txt_address);
        txt_email = (TextView)findViewById(R.id.txt_address);
        backBtn = (ImageView) findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.setPackage("com.google.android.gm");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"krishipahal@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "For Query");
                i.putExtra(Intent.EXTRA_TEXT   , "");

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }}
        });
        txt_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                // Send phone number to intent as data
                intent.setData(Uri.parse("tel:" + "++917869311456"));
                // Start the dialer app activity with number
                startActivity(intent);
            }
        });

    }


    private void back(){

        Intent intent = new Intent(context,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

        back();

    }
}
