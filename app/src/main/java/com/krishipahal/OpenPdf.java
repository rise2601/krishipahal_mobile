package com.krishipahal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;


public class OpenPdf extends AppCompatActivity {

Context context;

ViewPager view_pager;
    private ProgressDialog pDialog;
    public static final String MY_PREFS_NAME = "Krishipahal";
    SharedPreferences preferences;
    String [] page_image;

    Button next,prev;
    String bookname = "";
    String bookid = "";

    TextView book_name;
    ImageView backBtn;

    RelativeLayout _content;
    LinearLayout no_content;
    TextView page_no;
//    TextView page_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_pdf);
        context = this;
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        bookid = getIntent().getStringExtra("bookid");
        bookname = getIntent().getStringExtra("bookname");
        book_name = (TextView)findViewById(R.id.book_name);
        _content = (RelativeLayout)findViewById(R.id._content);
        no_content = (LinearLayout)findViewById(R.id.no_content);
        backBtn = (ImageView) findViewById(R.id.backBtn);

        view_pager = (ViewPager)findViewById(R.id.view_pager);
        prev = (Button) findViewById(R.id.prev);
        next = (Button) findViewById(R.id.next);
//        page_no = (TextView)findViewById(R.id.page_no);
        book_name.setText(bookname);


        try{
            make_book_request();
        }catch (Exception e){
            e.printStackTrace();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_pager.setCurrentItem(getItem(-1), true);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_pager.setCurrentItem(getItem(+1), true);
            }
        });

    }
    private int getItem(int i) {
        return view_pager.getCurrentItem() + i;
    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void make_book_request() {
        showpDialog();
        Log.e("Helllooo123", Webservices_url.bookpages+bookid);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,Webservices_url.bookpages+bookid
                , null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Helllooo123", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    String status = jsonObject.getString("status");

                    if (status.equals("200")){
                        JSONArray data = jsonObject.getJSONArray("data");
                        page_image = new String[data.length()];
                        if (data.length()==0){
                            _content.setVisibility(View.GONE);
                            no_content.setVisibility(View.VISIBLE);
                        }
                        else {
                            no_content.setVisibility(View.GONE);
                            _content.setVisibility(View.VISIBLE);

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                page_image[i] = object.getString("page_image");
                            }
                            view_pager.setAdapter(new ViewPagerBookAdapter(context, page_image));
                        }
                    }
                    
                    else {
                        Toast.makeText(context, "Sever Error Please try again later...", Toast.LENGTH_SHORT).show();
                    }

                }

                catch (Exception e){
                    e.printStackTrace();
                }
                hidepDialog();
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                hidepDialog();
            }
        });

        AppController.getInstance().getRequestQueue().getCache().invalidate(Webservices_url.booklist, true);
        Volley.newRequestQueue(this).add(jsonObjReq);
    }

    private void back(){
        Intent intent = new Intent(context,Book_grid.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
        back();
    }
}
