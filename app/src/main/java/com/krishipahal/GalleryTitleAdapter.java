package com.krishipahal;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
public class GalleryTitleAdapter extends RecyclerView.Adapter<GalleryTitleAdapter.MyViewHolder> {
    ArrayList<GalleryTitleModel> galleryTitleList;
    Context context;
    ArrayList<GalleryPicsModel> galleryPicsList;
    public GalleryTitleAdapter(Context context, ArrayList<GalleryTitleModel> galleryTitleList) {
        this.context = context;
        this.galleryTitleList = galleryTitleList;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        //GridView galleryGrid;
        RecyclerView recycleView;
        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView)view.findViewById(R.id.tv_title);
            recycleView = (RecyclerView) view.findViewById(R.id.recycleView);
            //galleryGrid = (GridView) view.findViewById(R.id.galleryGrid);
        }
    }
    @Override
    public GalleryTitleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_gallery_title, parent, false);
        return new GalleryTitleAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(GalleryTitleAdapter.MyViewHolder holder, int position) {
        GalleryTitleModel galleryTitleModel = galleryTitleList.get(position);
        holder.tv_title.setText(galleryTitleModel.getTitle());
        galleryPicsList = new ArrayList<>();
        galleryPicsList = galleryTitleModel.getGalleryPics();
        setGridAdapter(holder, galleryPicsList);
    }
    @Override
    public int getItemCount() {
        return galleryTitleList.size();
    }
    private void setGridAdapter(GalleryTitleAdapter.MyViewHolder holder, ArrayList<GalleryPicsModel> galleryPicsList) {
        //holder.galleryGrid.setAdapter(new GalleryAdapter(context, galleryPicsList));
        //LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recycleView.setLayoutManager(new GridLayoutManager(context, 2));
        //holder.recycleView.setLayoutManager(layoutManager);
        holder.recycleView.setAdapter(new GalleryAdapter(context, galleryPicsList));
    }
}
