package com.krishipahal;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
public class ZoomGalleryImage extends AppCompatActivity {
    String imageViewValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_gallery_image);
        PhotoView photoView = (PhotoView) findViewById(R.id.photoView);
        imageViewValue = getIntent().getStringExtra("IMAGE");
        Picasso.with(this)
                .load(Webservices_url.gallery_image+imageViewValue)
                .into(photoView);
    }
}
